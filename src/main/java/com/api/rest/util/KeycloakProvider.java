package com.api.rest.util;

import org.jboss.resteasy.client.jaxrs.internal.ResteasyClientBuilderImpl;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;

public class KeycloakProvider {

    private static final String SERVER_URL = "http://localhost:9999/auth";
    private static final String REALM_NAME = "nestjs-realm-dev";
    private static final String REALM_MASTER = "master";
    private static final String ADMIN_CLI = "admin-cli";
    private static final String USER_CONSOLE_NAME = "admin"; // login en keycload
    private static final String USER_CONSOLE_PASSWORD = "admin"; // login en keycload
    private static final String CLIENT_SECRET = "pJu9Q1DxiwemgPpaawwEwhw8fuX3Nw5R"; // credencial de cliente reyno

    public static RealmResource getRealmResources() {
        Keycloak keycloak = KeycloakBuilder.builder()
                .serverUrl(SERVER_URL)
                .realm(REALM_MASTER)
                .clientId(ADMIN_CLI)
                .username(USER_CONSOLE_NAME)
                .password(USER_CONSOLE_PASSWORD)
                //.clientSecret(CLIENT_SECRET)
                .resteasyClient(new ResteasyClientBuilderImpl()
                        .connectionPoolSize(10)
                        .build()
                )
                .build();
        return keycloak.realm(REALM_NAME);
    }

    public static UsersResource getUserResource() {
        RealmResource realmResource = getRealmResources();
        return realmResource.users();
    }
}

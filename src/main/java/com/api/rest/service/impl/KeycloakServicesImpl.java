package com.api.rest.service.impl;

import com.api.rest.dto.UserDTO;
import com.api.rest.service.IKeycloakService;
import com.api.rest.util.KeycloakProvider;
import jakarta.ws.rs.core.Response;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@Slf4j
public class KeycloakServicesImpl implements IKeycloakService {

    /**
     * @Description: Lista todos los usuarios registrado en ese realm, como ser victor.quino y daniel.quino
     * @Return: List<UserRepresentation>
     * */
    @Override
    public List<UserRepresentation> findAllUsers() {
        log.info("findAllUsers");
        return KeycloakProvider.getRealmResources().users().list();
    }

    /**
     * @Description: Obtiene un usuario por username
     * @Return: List<UserRepresentation>
     * */
    @Override
    public List<UserRepresentation> searchUserByUsername(String username) {
        log.info("searchUserByUsername");
        return KeycloakProvider.getRealmResources().users().searchByUsername(username, true);
    }

    /**
     * @Description: Metodo para crear un usuario en keycloak
     * @Return: String
     * */
    @Override
    public String createUser(@NonNull UserDTO userDTO) {
        log.info("createUser");
        int status = 0;
        UsersResource usersResource = KeycloakProvider.getUserResource();

        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setFirstName(userDTO.getFirstName());
        userRepresentation.setLastName(userDTO.getLastName());
        userRepresentation.setEmail(userDTO.getEmail());
        userRepresentation.setUsername(userDTO.getUsername());
        userRepresentation.setEmailVerified(true); // para no verificar el email
        userRepresentation.setEnabled(true);

        Response response = usersResource.create(userRepresentation);
        status = response.getStatus();
        System.out.println("victor: " + response);
        if (status == 201) {
            String path = response.getLocation().getPath();
            String userId = path.substring(path.lastIndexOf("/") + 1);// toma la url y obtiene el ultimo string despuesd del ultimo "/"

            CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
            credentialRepresentation.setTemporary(false); //  para la credencial no se temporal
            credentialRepresentation.setType(OAuth2Constants.PASSWORD); // contrasenia de tipo OAuth2
            credentialRepresentation.setValue(userDTO.getPassword()); // asignamos el password

            usersResource.get(userId).resetPassword(credentialRepresentation); // obtenemos el usuario y le agregamos el credencial creado

            // ASIGNACION DE ROLES AL NUEVO USUARIO
            RealmResource realmResource = KeycloakProvider.getRealmResources();
            List<RoleRepresentation> roleRepresentations = null;
            if (userDTO.getRoles() == null || userDTO.getRoles().isEmpty()) {
                // si no tiene roles en el dto:
                roleRepresentations = List.of(realmResource.roles().get("user").toRepresentation());
            } else {
                // si tiene roles en el dto:
                roleRepresentations = realmResource.roles()
                        .list()
                        .stream()
                        .filter(role -> userDTO.getRoles().stream().anyMatch(roleName -> roleName.equalsIgnoreCase(role.getName())))
                        .toList();
            }
            realmResource.users().get(userId).roles().realmLevel().add(roleRepresentations);
            return "User created successfully!!";
        } else if(status == 409) {
            log.error("Error al crear el usuario. 409");
            return "User exist alredy!!";
        } else {
            log.error("Error al crear el usuario.");
            return "Error";
        }
    }

    /**
     * @Description: Metodo para eliminar un usuario
     * @Return: void
     * */
    @Override
    public void deleteUser(String userId) {
        KeycloakProvider.getUserResource().get(userId).remove();
    }

    /**
     * @Description: Metodo para crear un usuario en keycloak
     * @Return: void
     * */
    @Override
    public void updateUser(String userId, UserDTO userDTO) {
        // Creamos una contrasenia
        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setTemporary(false);
        credentialRepresentation.setType(OAuth2Constants.PASSWORD);
        credentialRepresentation.setValue(userDTO.getUsername());

        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setFirstName(userDTO.getFirstName());
        userRepresentation.setLastName(userDTO.getLastName());
        userRepresentation.setEmail(userDTO.getEmail());
        userRepresentation.setUsername(userDTO.getUsername());
        userRepresentation.setEmailVerified(true); // para no verificar el email
        userRepresentation.setEnabled(true);
        userRepresentation.setCredentials(Collections.singletonList(credentialRepresentation));

        UserResource userResource = KeycloakProvider.getUserResource().get(userId);
        userResource.update(userRepresentation);
    }
}
